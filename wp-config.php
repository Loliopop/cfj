<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'cfj' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'pd~)%Whjm#U-_D#Q2BP|65=b[WaNGp=Wj;JL Yvkrm0/s{@tpY5CRWAy^PmM]+3r' );
define( 'SECURE_AUTH_KEY',  '1<$^k2_9Mg4?s`-u_[6NJ0a@_iq?oXV^Nr~)6kx2e8N0eSW}$qna-}{]dEoXtH-l' );
define( 'LOGGED_IN_KEY',    '>-*JY!NU+]?JWKXv;shF6p~Wz@8Nk5$Olo?,c|GAjGg{w44*.`t(56xql?)x.+yJ' );
define( 'NONCE_KEY',        '.aH@zd=7Pra[q ];BFs=*Ixv+TD91dN~@0a[,&+M/s]=d67KN+)Y[Qi:HErh.,l]' );
define( 'AUTH_SALT',        '?(?fGL&syp&se1~(P:!}g_g,a)YF[5>,vPMJ~$!@Y]>8005iQem|=OV_CwWa0c3;' );
define( 'SECURE_AUTH_SALT', 'JA:RWJ?p$fy oo++{_V+r1J2?d?Wew%R@wqxc6oihG+iiksiHC3l1Man;1o$P;9;' );
define( 'LOGGED_IN_SALT',   ',z3bU}!EblD-~m8NxC$u}99Q1]Mn[j]5<(%4/}/t/z XfGbNQs!dQ[dE.#X!M[)T' );
define( 'NONCE_SALT',       'q<+Pn*eo3l%>howQj+/?O?S@eItBhj0|EQ%Hji]b(Qh)oKI}aCXy`:ux?~(|e^tZ' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'cf_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
