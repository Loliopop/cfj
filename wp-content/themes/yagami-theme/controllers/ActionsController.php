<?php 
/**
* Class name: ProjectsController()
*
* A controller class is composed of methods suffixed with "Action", and responsibles for the following tasks:
* - Render the correct Twig/Timber template for the current page
* - Do the business logic associated to the current page
* - Provide the datas to the Twig/Timber templates
*
* @author : Kévin Vacherot <kevinvacherot@gmail.com>
*/

namespace Controllers;

use \Timber;
use \Timber\PostQuery;
use \TimberPost;

class ActionsController extends AppController
{
	/**
	 * __Constructor:
	*
	* Call AppController::__construct to inherit AppController useful methods
	*
	* @return void
	*/
	public function __construct(){
		parent::__construct();
	}


	/**
	 * Method called by Router::routing()
	*
	* ArchiveAction() method renders <projects/archive.twig> and provide it some datas
	*
	* @return void
	*/
	public function archiveAction(){
		$this->render('actions/archive.twig', array(
			'posts' => new Timber\PostQuery()
		));
	}


	/**
	 * Method called by Router::routing()
	*
	* SingleAction() method renders <projects/single.twig> and provide it some datas
	*
	* @return void
	*/
	public function singleAction(){

		$args = array(
			'post_type' => 'actions',
			'post_status' => 'publish',
			'posts_per_page' => 2,
			'orderby' => 'rand',
			'meta_key'		=> 'action_current',
			'post__not_in' => array (get_the_ID()),
			'meta_value'	=> true
		);

		$randomPosts = new Timber\PostQuery($args);

		$this->render('actions/single.twig', array(
			'post' => new TimberPost(),
			'randomPosts' => $randomPosts,
		));
	}

}