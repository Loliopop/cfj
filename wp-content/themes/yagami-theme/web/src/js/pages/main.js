export default {
	init: (app, ScrollMagic) => {
		/*
		|
		| Constants
		|-----------
		*/
        const 
            $body = $('body')
        ;
        
        /*
        |
        | parallax item
        |-----------------
        */
        var controller = new ScrollMagic.Controller();
            
        var scene = new ScrollMagic.Scene({
            triggerElement: $('.parallax-item'),
            triggerHook: "onEnter",
            duration: $(document).height()
        })
        .setTween($('.parallax-item'), {y: -200})
        .addTo(controller)


        /*
		|
		| Header
		|-----------------
		*/
        var header = document.querySelector('#header');
        var burgerMenu = document.querySelector('.btn-menu');
        var lastScrollTop = 0;

        var headerTl = new TimelineMax({paused: true});
        headerTl.to(header, .3, {y: '-100%'}, 'start');
        headerTl.to(burgerMenu, .3, {y: '-110%'}, 'start');

        window.addEventListener("scroll", function(){ 
            var st = window.pageYOffset || document.documentElement.scrollTop; 
            if (st > lastScrollTop && window.pageYOffset > 200){
                headerTl.play();
            } else {
                headerTl.reverse();
            }
            lastScrollTop = st <= 0 ? 0 : st;
        }, false);


        /*
		|
		| Loader
		|-----------------
		*/

        $body.on('loaderEnd', () => console.log('ended'))
	}
}