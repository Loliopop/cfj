export default {
	init: (app, Swiper) => {

        var sliderActions = $('.actions').find('.swiper-container');

        var mySwiper = new Swiper(sliderActions, {
            speed: 400,
            spaceBetween: 15,
            slidesPerView: 3,
            allowSlidePrev: false,
            allowSlideNext: false,
            breakpoints: {
                992: {
                    slidesPerView: 1.2,
                    spaceBetween: 15,
                    allowSlidePrev: true,
                    allowSlideNext: true,                
                },
            }
        });

    }
}