export default {
	init: (app, Menu, Kira, ScrollMagic, Loader) => {
		/*
		|
		| Constants
		|-----------
		*/
        const 
            $body        = $('body'),
            $pageLoader  = $('.page-loader'),
            $menuWrapper = $('#mobile-menu'),
            $menuButton  = $('.btn-menu'),
            $menuBars    = $menuButton.find('.item-burger > span'),
            $menuLi      = $menuWrapper.find('ul li')
		;
		

        /*
		|
		| Loader
		|---------
        */
        new Loader($pageLoader, "", $pageLoader.find('.number'));

        const loaderTl = new TimelineMax({paused: true});

        loaderTl.to($pageLoader.find('.item-content'), 0.4, { autoAlpha: 0, ease: Power1.easeOut })
        loaderTl.to($pageLoader, 0.7, { y: '-100%', ease: Power1.easeOut })
        loaderTl.eventCallback('onComplete', function () {
            $pageLoader.hide();
        })

        loaderTl.addCallback(() => { 
            $('body').css({
                'overflow': 'scroll',
            })
        })


        $body.on('loaded', function () {
            loaderTl.play();
        });




        /*
		|
		| Menu
		|-------
        */
        const menu = new Menu($menuWrapper, $menuButton, {});

        var $splitItems = new SplitText($menuLi, {type:"lines", linesClass: "line line++"});
        $menuLi.find('.line').wrap('<div class="line-wrapper">');

        menu.menuTimeline
            .from($menuWrapper, 0.5, { autoAlpha: 0, ease: Power1.easeOut }, 'start')
            .to($menuBars.eq(1), 0.3, { autoAlpha: 0, 'background-color': 'white'}, 'start')
			.to($menuBars.eq(0), 0.5, { x: 0, y: 8, 'background-color': 'white',rotation: 45, ease: Power1.easeOut }, 'start')
            .to($menuBars.eq(2), 0.5, { x: 0, y: -8, 'background-color': 'white',rotation: -45, ease: Power1.easeOut }, 'start')
            .staggerFrom($splitItems.lines, 0.6, { autoAlpha: 0, y: 50, ease: Power1.easeOut }, '0.1', '+=0')
        
        menu.init();

        /*
		|
		| Kira
		|-------
        */
        const kira = new Kira(ScrollMagic, {
            debug: false,
            loadEvent: {
                domElement: $body,
                eventName: 'loaded'
            },
            optimize: true,
            options: {
                // start: '-=0.6',
                triggerHook: .8
            }
        });

        /*
		| fadeInUp.parallax.sm
		|-----------------------
        */
        kira.add('fadeInUp', ($item, timeline, start) => {
            timeline.from($item, 0.6, { y: 60, autoAlpha: 0 }, start)
        });

        /*
		| fadeInLeft
		|-----------------------
        */
        kira.add('fadeInLeft', ($item, timeline, start) => {
            timeline.from($item, 0.6, { x:'100%', ease: Expo.easeOut }, start)
        });

        /*
		| fadeInLeft
		|-----------------------
        */
        kira.add('fadeInRight', ($item, timeline, start) => {
            timeline.from($item, 0.6, { x:'-100%', ease: Expo.easeOut }, start)
        });

        /*
		| fadeInLeft.parallax.sm
		|-----------------------
        */
        kira.add('fadeInLeft.stagger.sm', ($item, timeline, start) => {
            timeline.staggerFrom($item.find('[data-stagger-item]'), 0.6, { autoAlpha: 0, x: 20, ease: Power1.easeOut }, '0.1', start)
        });

        /*
		| title.parallax
		|--------------------
        */
        kira.add('title.parallax', ($item, timeline, start) => {
            
            var mySplitText = new SplitText($item, {type:"lines", linesClass: "line line++"});

            $item.find('.line').wrap('<div class="line-wrapper">');

            timeline.staggerFrom(mySplitText.lines, 0.8, { y: 100, autoAlpha: 0, ease: Sine.easeOut }, .3, start)
        });

        kira.init();
	}
}