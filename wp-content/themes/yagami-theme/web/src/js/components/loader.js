/*===========================================================
*
* LOADER COMPONENT
*
===========================================================*/


export default class Loader {
    /*
    *
    * Constructor
    *
    */
    constructor($container, $elBar, $ElFade) {

        this.bindMethods();

        this.loaderContainer = $container;
        this.width = 100;
        this.perfData = window.performance.timing;
        this.EstimatedTime = -(this.perfData.loadEventEnd - this.perfData.navigationStart);
        this.realTime = parseInt((this.EstimatedTime/1000)%60)*100;
        this.time = this.realTime * 0.5;
        this.barContainer = $elBar;
        this.body = $('body');
        this.elfade = $ElFade;
        this.windowLoaded = false;

        this.init();
    }

    /*
	|
	| bind
	|--------------
	*/
	bindMethods(){
        this.load = this.load.bind(this);
		this.loaded = this.loaded.bind(this);
	}

    /*
    *
    * INIT
    *
    */
    init(){
        // this.load()
        $(window).on('load', () => this.windowLoaded = true)
        this.animate();
	}

    /*
    *
    * Animation
    *
    */
    animate(){

        const loaderTl = new TimelineMax();
        let bar = this.barContainer
        let container = this.loaderContainer
        let duree = (this.time) / 1000
        let width = this.width
        let fade = this.elfade;

        var number = {value: 0};
        loaderTl.to(number, duree, {value:"100", 
            onUpdate:function() { 
                const content = fade.hasClass('no-count') ? 'Envoi du formulaire de contact' : Math.floor(number.value);
                    
                fade[0].innerHTML = content;
                
            },
            onComplete: this.loaded
        });




    }

    /*
    *
    * WHEN LOAD -> DISPATCH EVENT
    *
    */
	load(){
		//this.dispachEvent(this.body, 'load');
	}

    /*
    *
    * WHEN FINISH -> DISPATCH EVENT
    *
    */
	loaded(){
        if (this.windowLoaded){
            this.dispachEvent(this.body, 'loaded');
        } else {
            this.dispachEvent(this.body, 'loaded');
            //$(window).on('load', () => this.dispachEvent(this.body, 'loaded'));
        }
	}

    /*
    *
    * DISPATCH EVENT HELPER
    *
    */
    dispachEvent($element, eventName, datas = null){
		var event = $.Event(eventName);

		if(datas !== null){
			$.each(datas, function(key, value){
				event[key] = value
			});
		}

		$element.trigger(event);
	}

}
